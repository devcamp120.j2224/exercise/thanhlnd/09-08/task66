package com.devcamp.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.demo.model.Ward;
import com.devcamp.demo.repository.IWardRepository;

@Service
public class WardService {
    @Autowired
    IWardRepository iWardRepository;

    public ArrayList<Ward> getWardList() {
        ArrayList<Ward> listWard = new ArrayList<>();
        iWardRepository.findAll().forEach(listWard::add);
        return listWard;
    }
}
