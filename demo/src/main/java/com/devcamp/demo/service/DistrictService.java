package com.devcamp.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.demo.model.District;
import com.devcamp.demo.repository.IDistrictRepository;

@Service
public class DistrictService {
    @Autowired
    IDistrictRepository iDistrictRepository;

    public ArrayList<District> getDistrictList() {
        ArrayList<District> listDistrict = new ArrayList<>();
        iDistrictRepository.findAll().forEach(listDistrict::add);
        return listDistrict;
    }

}
