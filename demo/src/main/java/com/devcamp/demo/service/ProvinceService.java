package com.devcamp.demo.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.demo.model.Province;
import com.devcamp.demo.repository.IProvinceRepository;

@Service
public class ProvinceService {
    @Autowired
    IProvinceRepository iProvinceRepository;

    public ArrayList<Province> getProvinceList() {
        ArrayList<Province> listProvince = new ArrayList<>();
        iProvinceRepository.findAll().forEach(listProvince::add);
        return listProvince;
    }
}
