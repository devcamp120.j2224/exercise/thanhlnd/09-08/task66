package com.devcamp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.demo.model.Province;

public interface IProvinceRepository extends JpaRepository<Province,Integer>{
  
}
