package com.devcamp.demo.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.demo.model.District;
import com.devcamp.demo.model.Province;
import com.devcamp.demo.model.Ward;
import com.devcamp.demo.repository.IDistrictRepository;
import com.devcamp.demo.repository.IProvinceRepository;
import com.devcamp.demo.repository.IWardRepository;
import com.devcamp.demo.service.DistrictService;
import com.devcamp.demo.service.ProvinceService;
import com.devcamp.demo.service.WardService;
import com.devcamp.demo.service.*;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ApiController {
    @Autowired
    IProvinceRepository iProvinceRepository;

    @Autowired
    IDistrictRepository iDistrictRepository;

    @Autowired
    IWardRepository iWardRepository;

    @Autowired
    ProvinceService provinceService;

    @Autowired
    DistrictService districtService;

    @Autowired
    WardService wardService;

    // Gọi all province
    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getAllProvince() {
        try {
            return new ResponseEntity<>(provinceService.getProvinceList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // Gọi all district
    @GetMapping("/districts")
    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            return new ResponseEntity<>(districtService.getDistrictList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // Gọi all ward
    @GetMapping("/wards")
    public ResponseEntity<List<Ward>> getAllWard() {
        try {
            return new ResponseEntity<>(wardService.getWardList(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tìm province theo id
    @GetMapping("/provinces/{id}")
    public ResponseEntity<Province> getAllProvinceById(@PathVariable("id") int id) {
        Optional<Province> provinces = iProvinceRepository.findById(id);
        if (provinces.isPresent()) {
            return new ResponseEntity<>(provinces.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tìm district theo id
    @GetMapping("/districts/{id}")
    public ResponseEntity<District> getAllDistrictById(@PathVariable("id") int id) {
        Optional<District> districts = iDistrictRepository.findById(id);
        if (districts.isPresent()) {
            return new ResponseEntity<>(districts.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tìm ward theo id
    @GetMapping("/wards/{id}")
    public ResponseEntity<Ward> getAllWardById(@PathVariable("id") int id) {
        Optional<Ward> wards = iWardRepository.findById(id);
        if (wards.isPresent()) {
            return new ResponseEntity<>(wards.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    // thêm mới provinces
    @PostMapping("/provinces")
    public ResponseEntity<Object> createProvince(@RequestBody Province provinces) {
        Optional<Province> province = iProvinceRepository.findById(provinces.getId());
        try {
            if (province.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" province already exsit ");
            } else {
                Province createProvince = iProvinceRepository.save(provinces);
                return new ResponseEntity<>(createProvince, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified province: " +
                            e.getCause().getCause().getMessage());
        }
    }

    // thêm mới district
    @PostMapping("/districts/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") int id, @RequestBody District districts) {
        Optional<Province> province = iProvinceRepository.findById(id);
        Optional<District> district = iDistrictRepository.findById(districts.getId());
        if (province.isPresent()) {
            if (district.isPresent()) {
                return null;
            } else {
                Province province1 = province.get();
                districts.setProvince(province1);
                District createDistrict = iDistrictRepository.save(districts);
                return new ResponseEntity<>(createDistrict, HttpStatus.CREATED);
            }
        } else {
            return null;
        }
    }

    // thêm mới ward
    @PostMapping("/wards/{id}")
    public ResponseEntity<Object> createWards(@PathVariable("id") int id, @RequestBody Ward wards) {
        Optional<District> district = iDistrictRepository.findById(id);
        Optional<Ward> ward = iWardRepository.findById(wards.getId());
        if (district.isPresent()) {
            if (ward.isPresent()) {
                return null;
            } else {
                District district1 = district.get();
                wards.setDistrict(district1);
                Ward createWard = iWardRepository.save(wards);
                return new ResponseEntity<>(createWard, HttpStatus.CREATED);
            }
        } else {
            return null;
        }
    }

    // update province
    @PutMapping("/provinces/{id}")
    public ResponseEntity<Object> updateProvinceById(@PathVariable("id") int id, @RequestBody Province provinces) {
        Optional<Province> province = iProvinceRepository.findById(id);
        if (province.isPresent()) {
            Province province1 = province.get();
            province1.setCode(provinces.getCode());
            province1.setName(provinces.getName());
            return new ResponseEntity<>(iProvinceRepository.save(province1), HttpStatus.OK);
        } else {
            return null;
        }
    }

    // update district
    @PutMapping("/districts/{id}")
    public ResponseEntity<Object> updateDistrictsById(@PathVariable("id") int id, @RequestBody District districts) {
        Optional<District> district = iDistrictRepository.findById(id);
        if (district.isPresent()) {
            District district1 = district.get();
            district1.setPrefix(districts.getPrefix());
            district1.setName(districts.getName());
            return new ResponseEntity<>(iDistrictRepository.save(district1), HttpStatus.OK);
        } else {
            return null;
        }
    }

    // update ward
    @PutMapping("/wards/{id}")
    public ResponseEntity<Object> updateWardById(@PathVariable("id") int id, @RequestBody Ward wards) {
        Optional<Ward> ward = iWardRepository.findById(id);
        if (ward.isPresent()) {
            Ward ward1 = ward.get();
            ward1.setPrefix(wards.getPrefix());
            ward1.setName(wards.getName());
            return new ResponseEntity<>(iWardRepository.save(ward1), HttpStatus.OK);
        } else {
            return null;
        }
    }

    // delete province
    @DeleteMapping("/provinces/{id}")
    public ResponseEntity<Province> deleteProvinceById(@PathVariable("id") int id) {
        try {
            iProvinceRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete district
    @DeleteMapping("/districts/{id}")
    public ResponseEntity<District> deleteDistrictById(@PathVariable("id") int id) {
        try {
            iDistrictRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete ward
    @DeleteMapping("/wards/{id}")
    public ResponseEntity<Ward> deleteWardById(@PathVariable("id") int id) {
        try {
            iWardRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
